package com.tru.userservice.service;

import com.tru.userservice.UserMicroserviceApplication;
import com.tru.userservice.model.User;
import com.tru.userservice.testsupport.AbstractServiceTest;
import com.tru.userservice.testsupport.Asserts;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static com.tru.userservice.testsupport.Asserts.assertUsersAreEqual;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

/**
 * @author Raghu
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = UserMicroserviceApplication.class)
@Transactional
public class UserServiceTest extends AbstractServiceTest {

    @Autowired
    private AuthorityService authorityService;

    @Autowired
    private UserService userService;

    @Before
    public void setUp() {
        createTestData(authorityService, userService);
    }

    @Test
    public void testFindByRegistrationConfirmationToken() {
        final User alice = userService.findUserByUsername("alice");
        final String confirmationToken = alice.getRegistrationConfirmationToken();

        final User foundUser = userService.findByRegistrationConfirmationToken(confirmationToken);
        Asserts.assertUsersAreEqual(foundUser, alice);
    }

    @Test
    public void testFindByRegistrationConfirmationToken_NotFound() {
        assertThat(userService.findByRegistrationConfirmationToken("invalid"), is(nullValue()));
    }

    @Test
    public void testFindUserByUsername_NotFound() {
        assertThat(userService.findUserByUsername("invalid"), is(nullValue()));
    }

    @Test
    public void testFindUserByUsername() {
        Asserts.assertUsersAreEqual(userService.findUserByUsername("alice"), alice);
    }
}