package com.tru.userservice.model;

import com.tru.userservice.api.enums.UserProjections;
import org.springframework.data.rest.core.config.Projection;
import org.springframework.hateoas.Identifiable;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author Raghu
 */
@Projection(name = UserProjections.Values.USER_EXCERPT_DATA, types = User.class)
public interface UserExcerptProjection extends Identifiable<Long> {
    String getUsername();

    LocalDate getRegistrationDate();

    LocalDateTime getLastLogin();

    String getEmail();
}
