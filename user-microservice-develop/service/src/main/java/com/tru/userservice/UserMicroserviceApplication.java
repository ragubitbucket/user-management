package com.tru.userservice;

import com.tru.userservice.application.DevelopmentProfileConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.List;

@SpringBootApplication
@PropertySource("userservice.properties")
@EnableSwagger2
public class UserMicroserviceApplication {

    private static final String API_KEY_HEADER_NAME = "x-api-key";

    @Value("${api.version}")
    private String apiVersion;

    public static void main(String[] args) {
        SpringApplication.run(
                new Object[]{
                        UserMicroserviceApplication.class,
                        DevelopmentProfileConfiguration.class}
                , args);
    }

    @Bean
    public Docket swaggerSettings() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .securitySchemes(Collections.singletonList(apiKey()))
                .securityContexts(Collections.singletonList(securityContext()))
                .useDefaultResponseMessages(false)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.tru"))
                .paths(PathSelectors.any())
                .build()
                .pathMapping("/");
    }

    private ApiKey apiKey() {
        return new ApiKey(API_KEY_HEADER_NAME, API_KEY_HEADER_NAME, "header");
    }


    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Swagger Toys'R'Us API")
                .description("Toys'R'Us User Service API")
                //.contact(new Contact("TestName", "http:/test-url.com", "test@test.de"))
                .version(apiVersion)
                .build();
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.regex("/anyPath.*"))
                .build();
    }

    List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope
                = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Collections.singletonList(
                new SecurityReference(API_KEY_HEADER_NAME, authorizationScopes));
    }
}
