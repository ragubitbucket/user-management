package com.tru.userservice.repository;

import com.tru.userservice.api._internal.RestApiConstants;
import com.tru.userservice.model.Authority;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

public interface AuthorityRepository extends PagingAndSortingRepository<Authority, Long> {
    Authority findByAuthority(@Param(RestApiConstants.AUTHORITY_PARAM) String authority);

    @RestResource(exported = false)
    @Override Iterable<Authority> findAll();
}
