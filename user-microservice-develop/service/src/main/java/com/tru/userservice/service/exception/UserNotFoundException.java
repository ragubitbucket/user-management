package com.tru.userservice.service.exception;

import javax.ws.rs.NotFoundException;

/**
 * @author Raghu
 */
public class UserNotFoundException extends NotFoundException {
    public UserNotFoundException(String message) {
        super(message);
    }
}
