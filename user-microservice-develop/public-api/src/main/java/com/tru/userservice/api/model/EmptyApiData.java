package com.tru.userservice.api.model;

import com.tru.userservice.api._internal.model.AbstractBaseApiData;
import com.tru.userservice.api.resources.EmptyResource;
import com.tru.userservice.api._internal.model.AbstractBaseApiData;
import com.tru.userservice.api.resources.EmptyResource;
import org.springframework.hateoas.Link;

/**
 * Empty API data class with no data fields. Used as dummy data type by {@link EmptyResource}s.
 *
 * @author Raghu
 * @see EmptyResource
 */
public class EmptyApiData extends AbstractBaseApiData<EmptyResource> {
    @Override
    protected EmptyResource createNewResource(Link self) {
        return new EmptyResource(self);
    }
}
