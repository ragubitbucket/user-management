package com.tru.userservice.api.enums;

/**
 * Enum to specify the sort direction when sorting a paged resource.
 *
 * @author Raghu
 */
public enum SortDirection {
    ASC, DESC;
}
